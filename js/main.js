$(function () {
    //// On page load, defining the functions and varibles below  :|
    
    //// These varibles have the current items values :|
    var currentItem = 'default';
    var currentWorkItem = '';
    var isContentSet = true;
    
    //// These varibles are the value of responsive menu css
    var rm = $('.responsiveMenu'), rmLeft, rmTop, rmWidth, rmHeight;
    
    //// These varibles are the value of responsive menu ul css
    var rmu = $('.responsiveMenu ul'),rmuHeight, rmuWidth, rmuLeft, rmuTop;

    //// This function get property name and the unit you need as parameters and returnthe converted value :|
    var valueConvertor = function (element, property, unit) {
        var windowVal = 0;

        //// Checking which unit you want to convert;
        switch (unit) {
            case 'vw':
                windowVal = parseFloat(window.innerWidth / 100);
                break;
            case 'vh':
                windowVal = parseFloat(window.innerHeight / 100);
                break;
            default:
                windowVal = 0;
                break;
        }

        //// Converting the value and returning it :|
        var value = element.css(property);
        value = (parseFloat(parseFloat(value) / windowVal) + unit).toString();
        return value;
    };

    //// These varibles are the value of shapes css
    var defaultShapeLeft = valueConvertor($('#defaultShape'), 'left', 'vw')
    var defaultShapeTop = valueConvertor($('#defaultShape'), 'top', 'vh')
    var aboutShapeRight = valueConvertor($('#aboutShape'), 'right', 'vw')
    var aboutShapeTop = valueConvertor($('#aboutShape'), 'top', 'vh')
    var howShapeLeft = valueConvertor($('#howShape'), 'left', 'vw')
    var howShapeTop = valueConvertor($('#howShape'), 'top', 'vh')
    var contactShapeLeft = valueConvertor($('#contactShape'), 'left', 'vw')
    var contactShapeTop = valueConvertor($('#contactShape'), 'top', 'vh');

    //// This function write the codes on the left section  :|
    var typeHiddenMsg = function (targetId, sentence, counter, interval) {
        if (counter == sentence.length)
            sentence += $(targetId).html();
        
        //// Sentences scroll animation :|
        if (counter >= 1 && counter % 1000 == 0)
            $('#target').animate({ top: counter >= 3000 ? '-=' + window.innerHeight / 4 : counter >= 1500 ? '-=' + window.innerHeight / 6 : '' }, 5000);
        
        //// Writing the current character :|
        $(targetId).html(sentence.substr(0, counter));
        
        setTimeout(function () {
            typeHiddenMsg(targetId, sentence, ++counter, interval);
        }, interval);
    };
    
    //// This function gets the value of the selected menu item and set it to right section :|
    var setContent = function setContent(selectedItem){
        //// First append the last value to its tag then append the new value to right section :|
        $('body #' + currentItem.toLowerCase() + 'Item').append($('.rightSection').children());
        $('.rightSection').append($(selectedItem).children());
    };

    var setWorksContent = function (selectedItem) {
        //// First append the last value to its tag then append the new value to right section :|
        if($('.rightSection').text() != '')
            $(currentWorkItem).append($('.rightSection').children());
        currentWorkItem = selectedItem;
        $('.rightSection').append($(selectedItem).children());
    };
    
    //// On menu item click, this function get the item value and set its content to right section :|
    var onMenuItemClick = function (itemVal) {
        if (currentWorkItem != '') {
            $(currentWorkItem).append($('.rightSection').children());
            currentWorkItem = '';
        }

        //// Checking which item clicked :|
        itemVal = itemVal.toLowerCase();
        isContentSet = true;
        switch (itemVal) {
            case 'works':
                //// Checking was it on the current menu item or not :|
                if (currentItem != 'works') {
                    setContent('#worksItem');

                    //// Animating the Shape :|
                    $('#worksShape').animate({ left: '-22vw', top: '20vh' }, 1000);
                }
                currentItem = 'works';
                break;

            case 'contact':
                if (currentItem != 'contact') {
                    setContent('#contactItem');
                    $('#contactShape').css({ left: contactShapeLeft, top: contactShapeTop });
                    $('#contactShape').animate({ left: '-25vw', top: '20vh' }, 1000);
                }
                currentItem = 'contact';
                break;

            case 'how':
                if (currentItem != 'how') {
                    $('#howShape').css({ left: howShapeLeft, top: howShapeTop });
                    setContent('#howItem');
                    $('#howShape').animate({ left: '-13vw', top: '12vh' }, 1000);
                }
                currentItem = 'how';
                break;

            case 'about':
                if (currentItem != 'about') {
                    $('#aboutShape').css({ right: aboutShapeRight, top: aboutShapeTop });
                    setContent('#aboutItem');
                    $('#aboutShape').animate({ left: '-24vw', top: '3vh' }, 1000);
                }
                currentItem = 'about';
                break;

            case 'rocksema':
                if (currentItem != 'default') {
                    $('#defaultShape').css({ left: defaultShapeLeft, top: defaultShapeTop });
                    setContent('#defaultItem');
                    $('#defaultShape').animate({ left: '-20vw', top: '15vh' }, 1000);
                }
                currentItem = 'default';
                break;

            default:
                if (currentItem != 'default') {
                    $('#defaultShape').css({ left: defaultShapeLeft, top: defaultShapeTop });
                    setContent('#defaultItem');
                    $('#defaultShape').animate({ left: '-20vw', top: '15vh' }, 1000);
                }
                currentItem = 'default';
                break;
        }
    };

    //// On works item click, this function get the item valu and set its content to right section :|
    var onWorksItemClick = function (itemVal) {
        $('#worksItem').append($('.rightSection').children());
        currentItem = '';
        itemVal = itemVal.toLowerCase().replace(' ', '');
        setWorksContent('#' + itemVal + 'Content');
    };
    
    //// This function clear the content of right section with animating the elements inside  :|
    var clearContent = function(){
        $('.rightSection').css('display', 'none');
        rm.animate({
            width: '300px',
            height: '300px',
            borderRadius: '50%',
            left: rmLeft,
            top: rmTop
        }, 800);
        
        $('.exitItem').animate({
            left: '-6vw',
            top: '-4vh'
        });
        
        rmu.animate({ 
            left: rmuLeft,
            top: rmuTop
        }, 800);
    };
    
    //// On exit item clicked, calling the clearContent function to exit from current item :|
    $('.exitItem').click(function(){
        isContentSet = false;
        clearContent();
    });
    
    //// On responsive menu item clicked, animating the menu and then set the content to right section :|
    $('.responsiveMenu li').click(function(){
        var txt = $(this).text();
        
        //// These Varibles are the value of responsive menu css
        rm = $('.responsiveMenu');
        //rmWidth = valueConvertor(rm, 'width', 'vw');
        //rmHeight = valueConvertor(rm, 'height', 'vh');
        rmLeft = valueConvertor(rm, 'left', 'vw');
        rmTop = valueConvertor(rm, 'top', 'vh');
        
        //// These Varibles are the value of responsive menu ul css
        rmu = $('.responsiveMenu ul');
        //rmuWidth = valueConvertor(rmu, 'width', 'vw');
        //rmuHeight = valueConvertor(rmu, 'height', 'vh');
        rmuLeft = valueConvertor(rmu, 'left', 'vw');
        rmuTop = valueConvertor(rmu, 'top', 'vh');
        
        rmu.animate({ 
            left: '40vw',
            top: '32vh',
            color: '#000'
        }, 1000);
        
        rm.animate({ 
            width: '100vw',
            height: '100vh',
            borderRadius: '0',
            top: '0',
            left: '0'
        }, 1200, function(){
            //// On callback
            
            $('.exitItem').animate({
                left: '2vw',
                top: '2vh'
            });
            $('.rightSection').css('display', 'block');
            onMenuItemClick(txt);
        });
    });

    //// This interval checks that exitItem is visible or not in diffrent sizes
    setInterval(function(){
        
        //// Checking is size less than 767px and the content has shown, the exitItem should be visible :|
        if(parseFloat(window.innerWidth) <= 767)
                if(isContentSet == true)
                    $('.exitItem').animate({
                        left: '2vw',
                        top: '2vh'
                    });
        
        //// Checking is size more than 768px, the exitItem shouldn't be visible and right section should be :| 
        if(parseFloat(window.innerWidth) >= 768)
            {
                $('.exitItem').animate({
                        left: '-6vw',
                        top: '-4vh'
                    });
                
                if($('.rightSection').css('display') == 'none')
                    {
                        $('.rightSection').css('display', 'block');
                        isContentSet = true;
                    }
            }
    }, 1500);
    
    //// On page load, calling this function to set the default content :|
    currentItem = 'default';
    setContent('#defaultItem');
    $('#defaultShape').animate({ left: '-20vw', top: '15vh'}, 1000);
    isContentSet = false;
    
    /// Calling this function to write codes on the left section :|
    typeHiddenMsg("#target", $("#hidden_message").html(), 0, 20);

    $('.listItem').click(function () {
        $('.rightSection').css('display', 'block');
        onWorksItemClick($(this).find('h1').text());
    });

    $('.item').click(function () {
        $('.rightSection').css('display', 'block');
        onMenuItemClick($(this).text());
    });
    
    /* End */
});