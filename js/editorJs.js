var typeHiddenMsg = function (targetId, sentence, counter, interval) {
    if (counter == sentence.length)
        sentence += $(targetId).html();
     
    if (counter >= 1 && counter % 1000 == 0)
        $('#target').animate({top: counter >= 3000 ? '-=80px' : counter >= 1500 ? '-=50px' : ''}, 5000);
     
    $(targetId).html(sentence.substr(0, counter));
     
    setTimeout(function () {
        typeHiddenMsg(targetId, sentence, ++counter, interval);
    }, interval);
}

var showMenuItem1 = function (){
    $('#parallelogram').an
};
$(function () {
    var currentItem = 'V';
    var isOncurrentItem = false;
    $('.rightSection').append($('.defaultInfo').html());
    typeHiddenMsg("#target", $("#hidden_message").html(), 0, 20);
    
    $('.menuTitles').click(function(){
        switch($(this).text())
        {
            case 'Works':
                if(currentItem != 'Works'){
                    isOnCurrentItem = false;
                    $('.rightSection').empty();
                    $('.rightSection').append($('#worksItem').html());
                    if(!isOnCurrentItem)
                        $('#worksParallelogram').animate({ left: '-22vw', top: '20vh'}, 1000);
                }
                currentItem = 'Works';
                isOnCurrentItem = true;
                break;
                
            case 'Contact':
                if(currentItem != 'Contact'){
                    isOnCurrentItem = false;
                    $('.rightSection').empty();
                    $('.rightSection').append($('#contactItem').html());
                    if(!isOnCurrentItem)
                        $('#contactParallelogram').animate({ left: '-22vw', top: '20vh'}, 1000);
                }
                currentItem = 'Contact';
                isOnCurrentItem = true;
                break;
                
            case 'How':
                if(currentItem != 'How'){
                    isOnCurrentItem = false;
                    $('.rightSection').empty();
                    $('.rightSection').append($('#howItem').html());
                    if(!isOnCurrentItem)
                        $('#howParallelogram').animate({ left: '-22vw', top: '20vh'}, 1000);
                }
                currentItem = 'How';
                isOnCurrentItem = true;
                break;
                
            case 'About':
                if(currentItem != 'About'){
                    isOnCurrentItem = false;
                    $('.rightSection').empty();
                    $('.rightSection').append($('#aboutItem').html());
                    if(!isOnCurrentItem)
                        $('#aboutParallelogram').animate({ left: '-22vw', top: '20vh'}, 1000);
                }
                currentItem = 'About';
                isOnCurrentItem = true;
                break;
                
            default:
                if(currentItem != 'V'){
                    isOnCurrentItem = false;
                    $('.rightSection').empty();
                    $('.rightSection').append($('#defaultInfo').html());
                    if(!isOnCurrentItem)
                        $('#defaultParallelogram').animate({ left: '-22vw', top: '20vh'}, 1000);
                }
                currentItem = 'V';
                isOnCurrentItem = true;
                break;
        }
    });
    
    
});
